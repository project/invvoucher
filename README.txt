
##Invitation Voucher Readme##

To Install:
Unpack to /%DRUPAL ROOT%/modules/ catalog.

To Configure:
Enable the module, and then go to administer/user management/Invitation Voucher.

To Use:
Select Send invitation voucher, select lucky one, and enter his email! :)

Intro:
This module adds posibility to administrator of site to send special invitation vouchers (like 13453423) to users. 
Registration at site is availible only with this voucher, limited in time and number of registrations.
It will be asked on registration_new_user page.

Feautures:
	- can change terms of registration of voucher before sending
	- can change number of registrations availible with this voucher up to infinity (so, you can gift it to several your friends to join you in your fun)
	- can view already sent vouchers and delete them
	- can change sending mail template and subject
	- can change registration page URL


Module idea and testing:
Sergiy Zaschipas (UACODERS)serge@uacoders.com

Programming and documenting:
Anton Levchenko (UACODERS) uac_anton@yahoo.com

Feel free to contact us with propositioins and comments